using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Net;
using System.ComponentModel;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using TMPro;

public class SendFactory : MonoBehaviour
{
    public TMP_InputField Phone;
    public TMP_InputField Name;
    public CartSlotsContainer cartSlotsContainer;

    public bool isOpened;
    public GameObject UISend;

    private void Start()
    {
        UISend.SetActive(false);
    }

    private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
    {
        // Get the unique identifier for this asynchronous operation.
        string token = (string)e.UserState;

        if (e.Cancelled)
        {
            Debug.Log("Send canceled " + token);
        }
        if (e.Error != null)
        {
            Debug.Log("[ " + token + " ] " + " " + e.Error.ToString());
        }
        else
        {
            Debug.Log("Message sent.");
        }
    }

    /*
    public void SendEmail()
    {
        MailMessage mail = new MailMessage();
        SmtpClient smtpClient = new SmtpClient("smtp.gmail.com");
        smtpClient.Timeout = 10000;
        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
        smtpClient.UseDefaultCredentials = false;
        smtpClient.Port = 587;

        mail.From = new MailAddress("mensonmaria@gmail.com");
        mail.To.Add(new MailAddress(recipientEmail.text));
        mail.Subject = "Test email unity app!";
        mail.Body = cartController.ToString();

        smtpClient.Credentials = new System.Net.NetworkCredential("mensonmariawork@gmail.com", "Work21091993");
        ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        };

        mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
        smtpClient.Send(mail);
    }
    */

    /*
    public void SendText(string phoneNumber)
    {
        MailMessage mail = new MailMessage();
        SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
        SmtpServer.Timeout = 10000;
        SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
        SmtpServer.UseDefaultCredentials = false;

        mail.From = new MailAddress("mensonmaria@gmail.com");

        mail.To.Add(new MailAddress(phoneNumber + "@txt.att.net"));//See carrier destinations below
                                                                   //message.To.Add(new MailAddress("5551234568@txt.att.net"));
        mail.To.Add(new MailAddress(phoneNumber + "@vtext.com"));
        mail.To.Add(new MailAddress(phoneNumber + "@messaging.sprintpcs.com"));
        mail.To.Add(new MailAddress(phoneNumber + "@tmomail.net"));
        mail.To.Add(new MailAddress(phoneNumber + "@vmobl.com"));
        mail.To.Add(new MailAddress(phoneNumber + "@messaging.nextel.com"));
        mail.To.Add(new MailAddress(phoneNumber + "@myboostmobile.com"));
        mail.To.Add(new MailAddress(phoneNumber + "@message.alltel.com"));
        mail.To.Add(new MailAddress(phoneNumber + "@mms.ee.co.uk"));



        mail.Subject = "Subject";
        mail.Body = "";

        SmtpServer.Port = 587;

        SmtpServer.Credentials = new System.Net.NetworkCredential("mensonmariawork@gmail.com", "MyPasswordGoesHere") as ICredentialsByHost; SmtpServer.EnableSsl = true;
        ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        };

        mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
        SmtpServer.Send(mail);
    }
    */

    public void OpenSendForm()
    {
        isOpened = !isOpened;
        if (isOpened)
        {
            UISend.SetActive(true);
        }
        else
        {
            UISend.SetActive(false);
        }
    }

    public void sendMail()
    {
        // Command-line argument must be the SMTP host.
        SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
        client.Credentials = new System.Net.NetworkCredential(
            "mensonmariawork@gmail.com",
            "work21091993");
        client.EnableSsl = true;
        // Specify the email sender.
        // Create a mailing address that includes a UTF8 character
        // in the display name.
        MailAddress from = new MailAddress(
            "mensonmariawork@gmail.com",
            Name.text,
            System.Text.Encoding.UTF8);
        // Set destinations for the email message.
        MailAddress to = new MailAddress("mensonmaria@gmail.com");
        // Specify the message content.
        MailMessage message = new MailMessage(from, to);
        message.Body = Phone.text + "This is a test email message sent by an application unity. ";
        message.BodyEncoding = System.Text.Encoding.UTF8;
        message.Subject = "test message Unity";
        message.SubjectEncoding = System.Text.Encoding.UTF8;
        // Set the method that is called back when the send operation ends.
        client.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
        // The userState can be any object that allows your callback
        // method to identify this send operation.
        // For this example, the userToken is a string constant.
        string userState = "test message1";
        client.SendAsync(message, userState);
    }

    public void HideCanvasSend()
    {
        isOpened = !isOpened;
        if (isOpened)
        {
            UISend.SetActive(true);
        }
        else
        {
            UISend.SetActive(false);
        }
    }
}
