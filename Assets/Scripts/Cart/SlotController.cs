using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SlotController : MonoBehaviour //IPointerClickHandler
{
    private Stack<Item> items = new Stack<Item>();
    public Stack<Item> Items
    {
        get
        {
            return items;
        }
        set
        {
            items = value;
        }
    }

    [SerializeField] public TextMeshProUGUI count;
    [SerializeField] public TextMeshProUGUI Pname;
    [SerializeField] public TextMeshProUGUI Pprice;
    [SerializeField] public GameObject icon;
    [SerializeField] public Sprite slotEmpty;
    [SerializeField] public Sprite slotHighlight;

    public bool isEmpty
    {
        get
        {
            return items.Count == 0;
        }
    }

    public Item CurrentItem
    {
        get
        {
            if (items.Count > 0)
            {
                Debug.Log(items);
                return items.Peek();
            }
            return null;
        }

    }

    public bool isAvailable
    {
        get
        {
            return CurrentItem.maxSize > items.Count;
        }
    }

    private void Start()
    {
        RectTransform slotRect = GetComponent<RectTransform>();
        RectTransform iconProd = icon.GetComponent<RectTransform>();
        RectTransform txtCount = count.GetComponent<RectTransform>();
        RectTransform txtName = Pname.GetComponent<RectTransform>();
        RectTransform txtPrice = Pprice.GetComponent<RectTransform>();

        int txtScaler = (int)(slotRect.sizeDelta.x * 0.60);
    }

    private void ChangeSprite(Sprite neutral, Sprite highlight)
    {
        icon.GetComponent<Image>().sprite = neutral;
        SpriteState st = new SpriteState();
        st.highlightedSprite = highlight;
        st.pressedSprite = neutral;
        GetComponent<Button>().spriteState = st;
    }


    public void AddItem(Item item)
    {
        items.Push(item);

        if (items.Count > 0)
        {
            count.text = items.Count.ToString();
            Pname.text = item.product_name;
            Pprice.text = (item.price * items.Count).ToString();
            icon.GetComponent<Image>().sprite = item.spriteNeutral;
        }

        //ChangeSprite(item.spriteNeutral, item.spriteHeighlighted);
        
    }


    public void AddItems(Stack<Item> items)
    {
        this.items = new Stack<Item>(items);
        count.text = items.Count > 0 ? items.Count.ToString() : string.Empty;
        ChangeSprite(CurrentItem.spriteNeutral, CurrentItem.spriteHeighlighted);
    }


    public void UseItem()
    {

        if (!isEmpty)
        {
            items.Pop();

            count.text = items.Count > 0 ? items.Count.ToString() : string.Empty;
            if (items.Count != 0)
            {
                Pprice.text = (int.Parse(Pprice.text) - (int.Parse(Pprice.text) / (items.Count + 1))).ToString();
            }
            else
            {
                Pname.text = "No name";
                Pprice.text = "0";
                count.text = "0";
            }

            if (isEmpty)
            {
                //ChangeSprite(slotEmpty, slotHighlight);
                icon.GetComponent<Image>().sprite = slotEmpty;
                CartSlotsContainer.EmptySlot++;

            }
        }
        Debug.Log("!");
    }

    //public void OnPointerClick(PointerEventData eventData)
    //{
     //   if (eventData.button == PointerEventData.InputButton.Right)
     //   {
     //       UseItem();          
      //  }
   // }

    public void ClearSlot()
    {
        items.Clear();
        ChangeSprite(slotEmpty, slotHighlight);
        count.text = string.Empty;
        Pname.text = string.Empty;
        Pprice.text = string.Empty;
    }
}
