using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class LoadTexture : MonoBehaviour
{
    public string urlTexture;

    // Start is called before the first frame update
    public void Start()
    {
        string textureURL = urlTexture;
        StartCoroutine(DownloadTexture(textureURL));
    }

    IEnumerator DownloadTexture(string textureURL)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(textureURL);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            this.gameObject.GetComponent<Renderer>().material.mainTexture = ((DownloadHandlerTexture)request.downloadHandler).texture;
        }
    }
}
