using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


public class Player : MonoBehaviour
{
    //Open-Close cart
    public CartSlotsContainer CartSlotsContainer;
    public bool isOpened;
    public GameObject CartCanvas;

    //Open-Close pop-up window
    public GameObject pop_up_panel;
    public TextMeshProUGUI textName;
    public TextMeshProUGUI textPrice;
    public TextMeshProUGUI textDescription;
    public GameObject icon;

    //Add item 
    private Camera mainCamera;

    private void Awake()
    {
        CartCanvas.SetActive(true);
    }

    // Start is called before the first frame update
    void Start()
    {
        CartCanvas.SetActive(false);
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        //Add item to cart
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 2))
            {              
                if (hit.collider.gameObject.GetComponent<Item>() != null)
                {
                    CartSlotsContainer.AddItem(hit.collider.gameObject.GetComponent<Item>());
                }
            }
        }
    }

    public void OpenCart()
    {
        //Open-Close cart
            isOpened = !isOpened;
            if (isOpened)
            {
                CartCanvas.SetActive(true);
            }
            else
            {
                CartCanvas.SetActive(false);
            }
    }

    //Show pop-up window
    private void FixedUpdate()
    {
        if (pop_up_panel.activeSelf)
        {
            pop_up_panel.SetActive(false);
        }

        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 2))
        {
            Item itemInfo = hit.collider.GetComponent<Item>();

            if (itemInfo != null)
            {
                pop_up_panel.SetActive(true);
                textName.text = itemInfo.product_name;
                textPrice.text = itemInfo.price.ToString();
                textDescription.text = itemInfo.description;
                icon.GetComponent<Image>().sprite = itemInfo.spriteNeutral;

            }
        }
    }
}
