using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShelvesController : MonoBehaviour
{
    public GameObject[] shelveslong;

    private void Start()
    {
        Instantiate(shelveslong[0], new Vector3(-1, 0.3f, 8.1f), Quaternion.identity);
        Instantiate(shelveslong[1], new Vector3(3.3f, 0.3f, 8.1f), Quaternion.identity);
        Instantiate(shelveslong[2], new Vector3(5.0f, 0.3f, 8.1f), Quaternion.identity);
        Instantiate(shelveslong[3], new Vector3(9.3f, 0.3f, 8.1f), Quaternion.identity);
        Instantiate(shelveslong[4], new Vector3(11.0f, 0.3f, 8.1f), Quaternion.identity);


    }
}
